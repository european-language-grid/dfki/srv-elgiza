# serve.py
from flask import Flask
from flask import render_template
from flask import request

# creates a Flask application, named app
app = Flask(__name__)

# a route where we will display a welcome message via an HTML template
@app.route("/", methods=['POST','GET'])
def hello():
    return render_template('index.html')

# run the application
if __name__ == "__main__":
    app.run(debug=True)